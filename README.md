# SW2 qa-challenge


The purpose of this exercise is to asses your skills with QA and system analysis. You will be creating an actionable testing plan for a specific area of an existing application. This short challenge will begin with a "collaboration session", via Teams. Next, you will be able to work, independently, to produce the final deliverable. You may ask questions at any time, during the exercise, including during the "independent work" portion. You may take as long as you need, but we reccomend no longer than 1 hour for the entire exercise.

**Goals**

* Understand the business requirements of a brand-new application.
* Produce an actionable testing plan for a speicfic area of the app.
* Work effectively with incomplete and/or vague business requirements.

**Background**

During this exercise, you will be required to analyze our "inspection management platform". This software is currently used to manage the process of _stormwater compliance_ in construction projects. The software includes features to manage users, clients, projects, documents, and inspections.

In this exercise, we will focus on the "Clients" area. For the purpose of this exercise, the "initial requirements" of this area are not very detailed. The requirements consist of a set of user stories, outlined below.

**User Stories**

As a user, I need to...

* View a list of clients.
* View the details of a client, such as name, address, phone.
* View the contacts (users) associated with a client.
* View the project associated with a client.
* Associate contacts with a client.
* Associate projects with a client.
* Remove contacts from a client.
* Remove projects from a client.
* Navigate to the "project details" page, from the "client details" page.

## Instructions

1. Check your email for the registration link. Register your account and log-in to the software.
2. Click the "Clients" page.
3. Design a manual testin plan for this page and any relevant sub-pages, such as the client details page.


## Deliverables

* One "manual testing plan" for the "Clients" area of the application, formatted the manner that is most-convenient to the analyst.

## Evaluation Rubric

During evaluation of your submission, we will focus on the following areas:

* Does the testing plan work to ensure the application displays accurate and relevant data?
* Coverage of user interactions, forms, inputs, and buttons
* Does the testing plan consider any edge cases? 
* Acknowledgement of functionality that was not listed on the initial requirements.
* Process, collaboration, problem-solving. How do the analyst work to fill gaps in knowledge/information/requirements?
